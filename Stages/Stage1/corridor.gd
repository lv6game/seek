extends Node

var interactables
var interactingBlock
var nichole
var motionBlocked = false
var inputBlocked = false

func _ready():
	$Interactable/FlowerPot.setMsg(["A flower pot...", "Er...", "A hole?", "Is it a mouse nest?"])
	$Interactable/PhotoMan.setMsg(["...A weird look man"])
	$Interactable/PhotoPot.setMsg(["Is that a flower pot in the photo?"])
	$Interactable/Containers.setMsg(["Oh...", "There's something inside!"])
	$Interactable/FlowerPot1.setMsg(["Is this the flower pot which is in the photo?"])
	interactables = $Interactable.get_children()
	nichole = $Nichole
	
func _input(event):
	if !inputBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
		inputBlocked = true
		if !motionBlocked:
			for i in interactables:
				if i.interactable:
					motionBlocked = true
					interactingBlock = i
		if motionBlocked:
			nichole.move(0, 0)
			var tmp = interactingBlock.next()
			if tmp == "done":
				motionBlocked = false
				interactingBlock = null
			print(motionBlocked)
			nichole.setText(tmp)
		var t = Timer.new()
		t.set_wait_time(0.1)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		inputBlocked = false

func _process(delta):
	if !inputBlocked && !motionBlocked:
		if Input.is_action_pressed("ui_up"):
			nichole.move(0, -150)
		elif Input.is_action_pressed("ui_down"):
			nichole.move(0, 150)
		elif Input.is_action_pressed("ui_left"):
			nichole.move(-150, 0)
		elif Input.is_action_pressed("ui_right"):
			nichole.move(150, 0)
		else:
			nichole.move(0, 0)

#	if motionBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
#		var tmp = interactingBlock.next()
#		print(tmp)
#		if tmp != "done":
#			motionBlocked = false
#		nichole.setText(tmp)
		
				
