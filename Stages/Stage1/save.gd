extends Node

const SAVE_PATH = "res://Stages/Stage1/save.json"
var globalVar

func save_game():
	globalVar = get_node("/root/globalVar")
	var save_dict = {}
	var nodes_to_save = get_tree().get_nodes_in_group('Persist')
	for node in nodes_to_save:
		save_dict[node.get_path()] = node.save()
		
	save_dict[globalVar.get_path()] = globalVar.save()
	
	var save_file = File.new()
	save_file.open(SAVE_PATH, File.WRITE)

	save_file.store_line(to_json(save_dict))
	save_file.close()
	
	