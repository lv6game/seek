extends Node

var interactables
var interactingBlock
var inputBlocked = false
var nichole
var motionBlocked = false

func _ready():
	$Interactable/CtrlPanel.setMsg(["A lot of dust here on it...", "How long it hasn't been used?"])
	$Interactable/PC.setMsg(["The computer is locked."])
	$Interactable/Locker.setMsg(["...The size is big enough to put me in..."])
	$Interactable/Refrigerator.setMsg(["Oh... There's some food inside."])
	$Interactable/ServerStack.setMsg(["A...", "disk?"])
	interactables = $Interactable.get_children()
	nichole = $Nichole
	
func _input(event):
	if !inputBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
		inputBlocked = true
		if !motionBlocked:
			for i in interactables:
				if i.interactable:
					motionBlocked = true
					interactingBlock = i
		if motionBlocked:
			nichole.move(0, 0)
			var tmp = interactingBlock.next()
			if tmp == "done":
				motionBlocked = false
				interactingBlock = null
			print(motionBlocked)
			nichole.setText(tmp)
		var t = Timer.new()
		t.set_wait_time(0.2)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		inputBlocked = false
	
	if Input.is_key_pressed(KEY_ESCAPE) || Input.is_key_pressed(KEY_X) || Input.is_key_pressed(KEY_0):
		if !$Pause.show:
			inputBlocked = true
			$Pause.show()
		else:
			inputBlocked = false
			$Pause.hide()
		

func _process(delta):
	if !inputBlocked && !motionBlocked:
		if Input.is_action_pressed("ui_up"):
			nichole.move(0, -150)
		elif Input.is_action_pressed("ui_down"):
			nichole.move(0, 150)
		elif Input.is_action_pressed("ui_left"):
			nichole.move(-150, 0)
		elif Input.is_action_pressed("ui_right"):
			nichole.move(150, 0)
		else:
			nichole.move(0, 0)

#	if motionBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
#		var tmp = interactingBlock.next()
#		print(tmp)
#		if tmp != "done":
#			motionBlocked = false
#		nichole.setText(tmp)