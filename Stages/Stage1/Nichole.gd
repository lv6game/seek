extends KinematicBody2D

var motion = Vector2(0, 0);
enum DIRECTION{UP, DOWN, RIGHT, LEFT} 
var currentDirection = DIRECTION.DOWN
var textBox
var text
#var interacting = false
#var text
#var i
#var total
#
func setText(text):
	
	if !textBox.visible:
		textBox.visible = true
	if text != "done":
		self.text.text = text
	else:
		textBox.visible = false
	
func _ready():
	text = $CanvasLayer/TextBox/Text
	textBox = $CanvasLayer/TextBox
	position = get_node("/root/globalVar").playerPos

	match get_node("/root/globalVar").direction:
		"UP":
			currentDirection = DIRECTION.UP
			$AnimatedSprite.play("iup")
		"DOWN":
			currentDirection = DIRECTION.DOWN
			$AnimatedSprite.play("idown")
		"LEFT":
			currentDirection = DIRECTION.LEFT
			$AnimatedSprite.play("ileft")
		"RIGHT":
			currentDirection = DIRECTION.RIGHT
			$AnimatedSprite.play("iright")
#
func move(x = 0, y = 0):
	motion.x = x
	motion.y = y
	if y > 0:
		currentDirection = DIRECTION.DOWN
		$AnimatedSprite.play("down")
	elif y < 0:
		currentDirection = DIRECTION.UP
		$AnimatedSprite.play("up")
	elif x > 0:
		currentDirection = DIRECTION.RIGHT
		$AnimatedSprite.play("right")
	elif x < 0:
		currentDirection = DIRECTION.LEFT
		$AnimatedSprite.play("left")
#
func _physics_process(delta):
#	if !interacting:
#		if Input.is_action_pressed("ui_up"):
#			moveBy = Vector2(0, -speed)
#			currentDirection = DIRECTION.UP
#			$AnimatedSprite.play("up")
#		elif Input.is_action_pressed("ui_down"):
#			moveBy = Vector2(0, speed)
#			currentDirection = DIRECTION.DOWN
#			$AnimatedSprite.play("down")
#		elif Input.is_action_pressed("ui_left"):
#			moveBy = Vector2(-speed, 0)
#			$AnimatedSprite.play("left")
#			currentDirection = DIRECTION.LEFT
#		elif Input.is_action_pressed("ui_right"):
#			moveBy = Vector2(speed, 0)
#			$AnimatedSprite.play("right")
#			currentDirection = DIRECTION.RIGHT
#		else:
#			moveBy = Vector2(0, 0)
#
	move_and_slide(motion)
	
	if motion == Vector2(0, 0):
		match currentDirection:
			UP:
				$AnimatedSprite.play("iup")
			DOWN:
				$AnimatedSprite.play("idown")
			LEFT:
				$AnimatedSprite.play("ileft")
			RIGHT:
				$AnimatedSprite.play("iright")
				
func save():
	var save_dict = {
		pos={
			x = position.x,
			y = position.y
		}
	}
	return save_dict

