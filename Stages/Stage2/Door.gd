extends Area2D

export var effectName = ""
export(String, FILE, ".tscn") var nextScene

func _ready():
	connect("body_entered", self, "_on_Area2D_body_entered")

func _on_Area2D_body_entered(body):
	print("Entered")
	get_node("/root/globalVar").nextScene = nextScene