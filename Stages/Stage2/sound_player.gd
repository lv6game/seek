extends Area2D

var area = false

func _input(event):
	if Input.is_key_pressed(KEY_SPACE):
		if area == true:
			var sound_player = get_node("/root/Room/sound_player")
			sound_player.stream = load("res://Stages/Stage2/sound/17804__jace__static.wav")
			sound_player.connect("finished", self, "play_second_sound")
			sound_player.play(0)

func _ready():
	connect("body_entered", self, "sound_play")
	connect("body_exited", self, "area_close")
	
func sound_play(body):
	if body.name == "Nichole":
		area = true
		
func area_close(body):
	if body.name == "Nichole":
		area = false
		
func play_second_sound():
	var sound_player = get_node("/root/Room/sound_player")
	sound_player.stream = load("res://Stages/Stage2/sound/haa1.wav")
	sound_player.disconnect("finished", self, "play_second_sound")
	sound_player.play(0)
