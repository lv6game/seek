extends AnimationPlayer

var globalVar

func _on_Fade_animation_finished(anim_name):
	globalVar = get_node("/root/globalVar")
	if anim_name == "In":
		$ColorRect.visible = false
	if anim_name == "Out":
		print(globalVar.nextScene)
		get_tree().change_scene(globalVar.nextScene)
