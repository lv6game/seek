extends Node

var interactables
var interactingBlock
var nichole
var motionBlocked = false
var inputBlocked = false
var sfx = {}

func _ready():
	get_node("/root/globalVar").playerPos = Vector2(717, 511)
	
	sfx["door_slam"] = "res://SFX/door_slam.wav"
	sfx["button"] = "res://SFX/button.wav"

	$Interactable/PC.setMsg(["Hide?..."])
	$Interactable/Bookshelf.setMsg(["Weird book...", "(pressed)"])
	$AutoInteract/DoorSlam.setMsg(["...Was it", "a door?..."])
	interactables = $Interactable.get_children()
	
	nichole = $Nichole
	nichole.position = get_node("/root/globalVar").playerPos
	
func _input(event):
	if !inputBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
		inputBlocked = true
		print($Interactable/PC.interactable)
		if !motionBlocked:
			for i in interactables:
				if i.interactable:
					motionBlocked = true
					interactingBlock = i
		if motionBlocked:
			nichole.move(0, 0)
			var tmp = interactingBlock.next()
			if tmp == "(pressed)":
				$AudioPlayer.stream = load(sfx["button"])
				$AudioPlayer.play()
			if tmp == "done":
				motionBlocked = false
				interactingBlock = null
			print(motionBlocked)
			nichole.setText(tmp)
		var t = Timer.new()
		t.set_wait_time(0.1)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		inputBlocked = false
		
func interact(block):
	inputBlocked = true
	motionBlocked = true
	interactingBlock = block
	
	match block.name:
		"DoorSlam": 
			$AudioPlayer.stream = load(sfx["door_slam"])
			$AudioPlayer.play()

	if motionBlocked:
		nichole.move(0, 0)
		
		nichole.setText(interactingBlock.next())
	
	var t = Timer.new()
	t.set_wait_time(0.1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	inputBlocked = false

func _process(delta):
	if !inputBlocked && !motionBlocked:
		if Input.is_action_pressed("ui_up"):
			nichole.move(0, -150)
		elif Input.is_action_pressed("ui_down"):
			nichole.move(0, 150)
		elif Input.is_action_pressed("ui_left"):
			nichole.move(-150, 0)
		elif Input.is_action_pressed("ui_right"):
			nichole.move(150, 0)
		else:
			nichole.move(0, 0)

#	if motionBlocked && (Input.is_key_pressed(KEY_SPACE) || Input.is_key_pressed(KEY_ENTER) || Input.is_key_pressed(KEY_Z)):
#		var tmp = interactingBlock.next()
#		print(tmp)
#		if tmp != "done":
#			motionBlocked = false
#		nichole.setText(tmp)
		
				
