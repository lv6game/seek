extends Node

var current_area = ""
var door_slam_played = false
onready var sound_player = get_node("/root/Room/sound_player")
onready var text_box = get_node("/root/Room/Nichole/Camera2D/Textbox")
onready var text_show = get_node("/root/globalVar")
onready var NicholeIcon = get_node("/root/Room/Nichole/Camera2D/Textbox/Icon")
onready var computer_sheet = get_node("/root/Room/object/Computer_sheet")
onready var time = get_node("/root/Room/Timer")
var unlock = false
var door_unlock = false

func open_text_box(text):
	NicholeIcon.visible = true
	text_box.visible = true
	text_box.get_node("Text").text = text
	text_show.text_show = true

func close_text_box():
	NicholeIcon.visible = false
	text_box.visible = false
	text_box.get_node("Text").text = ""
	text_show.text_show = false

func _input(event):
	if Input.is_key_pressed(KEY_SPACE):
		if current_area == "door_slam":
			close_text_box()
			door_slam_played = true
		elif current_area == "door_slam_left":
			if text_box.visible == true:
				close_text_box()
				door_slam_played = true
				
		if current_area == "computer":
			if text_show.text_show == false:
				computer_sheet.visible = true
				
				time.set_wait_time(2)
				time.connect("timeout",self,"computer_sheet_closed")
				time.start()
			elif text_show.text_show == true:
				close_text_box()
				current_area = ""
		elif current_area == "computer_left":
			if text_box.visible == true:
				close_text_box()
				
		if current_area == "bookself" and unlock == false:
			if text_show.text_show == false:
				open_text_box("呢本書好奇怪哦!好像被人整過來")
			elif text_show.text_show == true:
				if text_box.get_node("Text").text == "呢本書好奇怪哦!好像被人整過來":
					sound_player.stream = load("res://Stages/Stage2/sound/105379__drzoom__open-lever-on-capsule-coffee-machine.wav")
					sound_player.play(0)
					
					text_box.get_node("Text").text = "咔嚓...(按下去)"
				else:
					close_text_box()
					current_area = ""
					unlock = true
		elif current_area == "bookself_left":
			if text_box.visible == true:
				close_text_box()
				
		if current_area == "key" and unlock == true and door_unlock == false:
			if text_show.text_show == false:
				open_text_box("好似個裂縫有野窩!")
			elif text_show.text_show == true:
				if text_box.get_node("Text").text == "好似個裂縫有野窩!":
					text_box.get_node("Text").text = "啊!我只手指啊!(流血中...)(機關開啟聲音)"
					
					sound_player.stream = load("res://Stages/Stage2/sound/uu.wav")
					sound_player.play(0)
				elif text_box.get_node("Text").text =="啊!我只手指啊!(流血中...)(機關開啟聲音)":
					close_text_box()
					current_area = ""
					
					sound_player.stream = load("res://Stages/Stage2/sound/wara1 (1).wav")
					sound_player.play(0)
					
					door_unlock = true
		elif current_area == "key_left":
			if text_box.visible == true:
				close_text_box()
				
		if current_area == "sound_player":
			if text_show.text_show == false:
				sound_player.stream = load("res://Stages/Stage2/sound/17804__jace__static.wav")
				sound_player.play(0)
				sound_player.connect("finished", self, "radio_on")
			elif text_show.text_show == true:
				close_text_box()
				current_area = ""
		elif current_area == "sound_player_left":
			if text_box.visible == true:
				close_text_box()
				
		if current_area == "paper":
			if text_show.text_show == false:
				get_node("/root/Room/object/Paper").visible = true
				
				time.set_wait_time(2)
				time.start()
				time.connect("timeout", self, "after_paper_show")
			elif text_show.text_show == true:
				close_text_box()
				current_area = ""
		elif current_area == "paper_left":
			if text_box.visible == true:
				close_text_box()
				current_area = ""
			
func after_paper_show():
	get_node("/root/Room/object/Paper").visible = false
	time.disconnect("timeout", self, "after_paper_show")
	open_text_box("張紙究竟想表達咩呢??????")
			
func radio_on():
	sound_player.stream = load("res://Stages/Stage2/sound/haa1.wav")
	sound_player.play(0)
	sound_player.disconnect("finished", self ,"radio_on")
	sound_player.connect("finished", self, "after_radio_play")
	
func after_radio_play():
	sound_player.disconnect("finished", self , "after_radio_play")
	open_text_box("點解有一個女仔錄音 聽落去好似被人追甘 唔通呢度有其他女仔系度")
	

func _ready():
	$Television.connect("body_entered", self, "on_televison_entered")
	$Television.connect("body_exited", self, "on_televison_left")
	$Key.connect("body_entered", self, "on_key_entered")
	$Key.connect("body_exited", self, "on_key_left")
	$sound_player.connect("body_entered", self, "on_sound_player_entered")
	$sound_player.connect("body_exited", self, "on_sound_player_left")
	$paper.connect("body_entered", self, "on_paper_entered")
	$paper.connect("body_exited", self, "on_paper_left")
	
func on_televison_entered(body):
	if body.name == "Nichole":
		current_area = "television"
		var television = get_node("/root/Room/object/Television")
		television.play("default")
		
func on_computer_entered(body):
	if body.name == "Nichole":
		current_area = "computer"
		
func on_bookself_entered(body):
	if body.name == "Nichole":
		current_area = "bookself"
		
func on_key_entered(body):
	if body.name == "Nichole":
		current_area = "key"
		
func on_sound_player_entered(body):
	if body.name == "Nichole":
		current_area = "sound_player"
		
func on_paper_entered(body):
	if body.name == "Nichole":
		current_area = "paper"

func on_televison_left(body):
	if body.name == "Nichole":
		current_area = "television_left"
	
func on_Door_slam_sound_left(body):
	if body.name == "Nichole":
		current_area = "door_slam_left"

func on_computer_left(body):
	if body.name == "Nichole":
		current_area = "computer_left"
		
func on_bookself_left(body):
	if body.name == "Nichole":
		current_area = "bookself_left"
		
func on_key_left(body):
	if body.name == "Nichole":
		current_area = "key_left"
		
func on_sound_player_left(body):
	if body.name == "Nichole":
		current_area = "sound_player_left"

func on_paper_left(body):
	if body.name == "Nichole":
		current_area = "paper_left"

	