extends Button

export var effectName = ""
export(String, "In", "Out") var inOrOut

func _ready():
	connect("pressed", self, "change_scene")
	
func change_scene():
	get_node("/root/Node/Fade/ColorRect").visible = true
	get_node("/root/globalVar").nextScene = "res://Stages/Stage2/Room.tscn"
	get_node("/root/Node/Fade").play("Out")
