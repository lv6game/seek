extends KinematicBody2D

var moveBy = Vector2(0, 0);
enum DIRECTION{UP, DOWN, RIGHT, LEFT} 
var currentDirection = DIRECTION.DOWN

func _physics_process(delta):
	var Speed = 120
	
	if Input.is_action_pressed("ui_up"):
		moveBy = Vector2(0, -Speed)
		currentDirection = DIRECTION.UP
		$AnimatedSprite.play("up")
	elif Input.is_action_pressed("ui_down"):
		moveBy = Vector2(0, Speed)
		currentDirection = DIRECTION.DOWN
		$AnimatedSprite.play("down")
	elif Input.is_action_pressed("ui_left"):
		moveBy = Vector2(-Speed, 0)
		$AnimatedSprite.play("left")
		currentDirection = DIRECTION.LEFT
	elif Input.is_action_pressed("ui_right"):
		moveBy = Vector2(Speed, 0)
		$AnimatedSprite.play("right")
		currentDirection = DIRECTION.RIGHT
	else:
		match currentDirection:
			UP:
				$AnimatedSprite.play("iup")
			DOWN:
				$AnimatedSprite.play("idown")
			LEFT:
				$AnimatedSprite.play("ileft")
			RIGHT:
				$AnimatedSprite.play("iright")
		moveBy = Vector2(0, 0)
	
	self.move_and_slide(moveBy)
