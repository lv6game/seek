extends CanvasLayer

var show = false
var tabIndex = 0
var tabs

func _ready():
	tabs = [$Pause/Inventory, $Pause/Save, $Pause/Exit]
	get_node("/root/globalVar").inventory.append("lol")
	for i in get_node("/root/globalVar").inventory:
		$Pause/ItemList.add_item(i)
		print(i)
	
func _input(event):
	if event is InputEventKey:
		match event.scancode:
			KEY_LEFT:
				if tabIndex > 0:
					tabIndex -= 1
			KEY_RIGHT:
				if tabIndex < 2:
					tabIndex += 1

func show():
	$Pause.show()
	show = true
	
func hide():
	$Pause.hide()
	show = false