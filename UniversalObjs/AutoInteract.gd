extends Area2D

var interactable = false
var index = -1
var messages = [""]
export(String) var item = null

func setMsg(msg):
	messages = msg

func next():
	index += 1
	if index >= messages.size():
		index = -1
		if item != null:
			get_node("/root/globalVar").inventory.append(item)
			$Shape.disabled = true
			return "remove"
		else:
			$Shape.disabled = true
			return "done"

	return messages[index]


func _on_AutoInteract_body_exited(body):
	if body.name == "Nichole":
		get_node("/root/Node").interact(self);
