extends Area2D

export var effectName = ""
export(String, FILE, ".tscn") var nextScene
export(Vector2) var nextPos
export(String, "UP", "DOWN", "LEFT", "RIGHT") var direction

func _ready():
	connect("body_entered", self, "_on_Area2D_body_entered")

func _on_Area2D_body_entered(body):
	print("Entered")
	get_node("/root/globalVar").nextScene = nextScene
	get_node("/root/globalVar").playerPos = nextPos
	get_node("/root/globalVar").direction = direction
	if body.name == "Nichole":
		$AnimatedSprite.play("opened")
		get_node("/root/Node/" + effectName + "/ColorRect").visible = true
		get_node("/root/Node/" + effectName).play("Out")
