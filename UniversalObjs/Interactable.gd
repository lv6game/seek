extends Area2D

#interactable checks player to see if they are nearby the obj or not
#so the chat box can be called
var interactable = false
var index = -1
var messages = [""]
export(String) var item = null

func setMsg(msg):
	messages = msg

func next():
	index += 1
	if index >= messages.size():
		index = -1
		if item != null:
			get_node("/root/globalVar").inventory.append(item)
			return "remove"
		else:
			return "done"

	return messages[index]
	
func _process(delta):
	for i in get_overlapping_bodies():
		if i.name  == "Nichole":
			interactable = true
		print(interactable)


func _on_Interactable_body_exited(body):
	interactable = false
